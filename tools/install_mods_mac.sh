#!/bin/bash
set -o nounset
set -o pipefail
set -o errexit

FABRIC_URL="https://maven.fabricmc.net/net/fabricmc/fabric-installer/0.6.1.45/fabric-installer-0.6.1.45.jar"
MODS_URL="https://gitlab.com/avalonparton/theboys/-/raw/master/mods.zip"
MC_VERSION="1.16.1"
FABRIC_VERSION="0.9.0+build.204-1.16.1"

# Download and install Fabric if not installed
if [[ -d "~/Library/Application Support/minecraft/.fabric/remappedJars/minecraft-${MC_VERSION}/intermediary-fabric-loader-${FABRIC_VERSION}.jar" ]]; then
    echo "Fabric already installed."
else
    if ! java --version; then
        echo "Please install java JRE"
        exit 1
    else
    echo "Installing Fabric using universal .jar installer."
    curl --silent --output fabric-installer.jar "${FABRIC_URL}"
    java -jar fabric-installer.jar client -dir "${HOME}/Library/Application Support/minecraft"
    rm -v fabric-installer.jar
fi

# Update the mods.
curl --silent --output mods.zip "${MODS_URL}"
mkdir -p "${HOME}/Library/Application Support/minecraft/mods"
echo "Cleaning up old mods"
rm -v "${HOME}/Library/Application Support/minecraft/mods/*.jar" || true
unzip mods.zip -d "${HOME}/Library/Application Support/minecraft/mods/"
rm -v mods.zip

echo "Successfully installed mods!"
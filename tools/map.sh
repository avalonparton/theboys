#!/bin/bash
set -o errexit
set -o pipefail
set -x

# Directory setup
DIR="$( cd "$(dirname "$0")" ; pwd -P )"
WORLD_DIR="${DIR}/../map/world"
MAP_DIR="${DIR}/../map/output"
mkdir --parents --verbose "${WORLD_DIR}" "${MAP_DIR}"

# FTP setup
FTP_HOST="na329.pebblehost.com"
FTP_USERNAME="avalonlee@gmail.com.113028"
FTP_PASSWORD="${1}"
[[ "${FTP_PASSWORD}" == "" ]] && echo "Usage: ${0} [ftp password]" && exit 1


# Check if anybody is online
NUM_PLAYERS=$(python3 ${DIR}/player_count.py)
if [[ "${NUM_PLAYERS}" != "0" ]]; then
    echo "There are ${NUM_PLAYERS} player(s) online. Stopping."
    #exit 1
fi

# Copy the world files from the server
lftp "${FTP_HOST}" << COMMAND
user ${FTP_USERNAME} "${FTP_PASSWORD}"
mirror --verbose --only-newer --parallel=12 /world ${WORLD_DIR}
COMMAND

# Generate the map using Overviewer
time overviewer.py "${WORLD_DIR}" "${MAP_DIR}"

# TODO: move this into a service
# Start the HTTP server
ulimit -n 8192
caddy --root "${MAP_DIR}" --port 42069
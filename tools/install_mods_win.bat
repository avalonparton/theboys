@echo off
:: Make sure fabric is installed
IF EXIST %APPDATA%\.minecraft\.fabric\remappedJars\minecraft-1.16.1 (
    echo Fabric already installed for 1.16.1! 
) ELSE (
    echo Downloading and installing fabric for 1.16.1
    powershell.exe -Command "$ProgressPreference = 'SilentlyContinue'; Invoke-WebRequest -ContentType 'application/octet-stream' -OutFile %TEMP%\fabric-installer.exe 'https://maven.fabricmc.net/net/fabricmc/fabric-installer/0.6.1.45/fabric-installer-0.6.1.45.exe'" || (echo Failed to download Fabric installer from URL: https://maven.fabricmc.net/net/fabricmc/fabric-installer/0.6.1.45/fabric-installer-0.6.1.45.exe && exit /b)
    %TEMP%\fabric-installer.exe client -dir %APPDATA%\.minecraft || (echo Failed to install fabric profile && exit /b)
)

:: Create the mods folder
IF NOT EXIST %APPDATA%\.minecraft\mods (
    mkdir %APPDATA%\.minecraft\mods\
)

:: Delete mods.zip
IF EXIST %TEMP%\mods.zip (
    del /S /q %TEMP%\mods.zip
)

echo Downloading mods (this may take a while)
powershell.exe -Command "$ProgressPreference = 'SilentlyContinue'; Invoke-WebRequest -ContentType 'application/octet-stream' -OutFile %TEMP%\mods.zip 'https://gitlab.com/avalonparton/theboys/-/raw/master/mods.zip'" || (echo Failed to download mods from URL: https://gitlab.com/avalonparton/theboys/-/raw/master/mods.zip && exit /b)

echo Deleting old mods
del /S /q %APPDATA%\.minecraft\mods\*.jar || (echo Failed to delete old mods %TEMP%\mods.zip && exit /b)

echo Unzipping new mods
powershell.exe Expand-Archive %TEMP%\mods.zip -DestinationPath %APPDATA%\.minecraft\mods\ || (echo Failed to unzip %TEMP%\mods.zip && exit /b)

echo Deleting mods.zip
del /S /q %TEMP%\mods.zip || exit /b

echo Successfully updated mods!
pause

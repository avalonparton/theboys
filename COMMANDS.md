# Command Block commands

## Elytra Shop
```python
# doesn't work, maybe too long?
# execute as @p if entity @p[nbt={Inventory:[{id:"minecraft:feather",Count:64b}, {id:"minecraft:leather",Count:64b}, {id:"minecraft:chorus_fruit",Count:64b}, {id:"minecraft:phantom_membrane",Count:10b}, {id:"minecraft:diamond",Count:1b}]}] run clear @p minecraft:feather 64

# Impulse, Unconditional, Needs redstone
execute as @p if entity @p[nbt={Inventory:[{id:"minecraft:feather",Count:64b}, {id:"minecraft:leather",Count:64b}, {id:"minecraft:chorus_fruit",Count:64b}]}] run say I believe I can fly

# Chain, Conditional, Always on
execute as @p if entity @p[nbt={Inventory:[{id:"minecraft:phantom_membrane",Count:10b}, {id:"minecraft:diamond",Count:1b}]}] run say and I have the goods
clear @p minecraft:feather 64
clear @p minecraft:leather 64
clear @p minecraft:chorus_fruit 64
clear @p minecraft:phantom_membrane 10
clear @p minecraft:diamond 1
give @p minecraft:elytra 1
```

## Head Shop
execute as @p if entity @s[nbt={Inventory:[{id:"minecraft:leather",Count:64b}]}] run give @s minecraft:player_head{SkullOwner:"plomdawg"}

### Monster Heads
execute as @p if entity @s[nbt={Inventory:[{id:"minecraft:leather",Count:64b}, {id:"minecraft:bone",Count:64b}]}] run give @p minecraft:skeleton_skull
clear @p minecraft:bone 64
clear @p minecraft:leather 64

execute as @p if entity @s[nbt={Inventory:[{id:"minecraft:leather",Count:64b}, {id:"minecraft:rotten_flesh",Count:64b}]}] run give @p minecraft:zombie_head
clear @p minecraft:rotten_flesh 64
clear @p minecraft:leather 64

execute as @p if entity @s[nbt={Inventory:[{id:"minecraft:leather",Count:64b}, {id:"minecraft:gunpowder",Count:64b}]}] run give @p minecraft:creeper_head
clear @p minecraft:gunpowder 64
clear @p minecraft:leather 64

## Server Settings

```
motd=The boys are \u00A7bsurviving\u00A7f\!
pvp=true
difficulty=hard
view-distance=10
max-build-height=256
```
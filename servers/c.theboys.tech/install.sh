#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"


# Stop server is it's running.
if systemctl is-active --quiet minecraft-c; then
    echo "stopping server"
    sudo systemctl stop minecraft-c
fi

sudo cp --verbose "${DIR}/minecraft-c.service" /etc/systemd/system/minecraft-c.service
sudo systemctl enable minecraft-c
sudo systemctl start minecraft-c
sudo systemctl status minecraft-c --no-pager

echo "installed service minecraft-c"
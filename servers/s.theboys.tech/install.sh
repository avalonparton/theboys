#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"


# Stop server is it's running.
if systemctl is-active --quiet minecraft-s; then
    echo "stopping server"
    sudo systemctl stop minecraft-s
fi

sudo cp --verbose "${DIR}/minecraft-s.service" /etc/systemd/system/minecraft-s.service
sudo systemctl enable minecraft-s
sudo systemctl start minecraft-s
sudo systemctl status minecraft-s --no-pager

echo "installed service minecraft-s"
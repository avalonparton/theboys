# theboys

MC Server with the boys

# Setting up Steve

This is how I set up Steve, the NUC that runs my creative minecraft server.

```
sudo apt update
sudo apt install --yes openjdk-14-jre-headless make git gcc

# Install MCRCON
git clone https://github.com/Tiiffi/mcrcon.git
pushd mcrcon
make
sudo make install
popd

git clone https://gitlab.com/avalonparton/theboys.git
cd theboys/servers/c.theboys.tech
bash ./install.sh
```